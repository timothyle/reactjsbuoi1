import React, { Component } from 'react'

export default class ItemBai extends Component {
  imgSrc = "https://edgewoodreit.com/wp-content/uploads/2018/01/500x325.png";
  render() {
    return (
      <div class="container px-lg-5">
                <div class="row gx-lg-5">
                    <div class="col-lg-4 col-xxl-3 mb-5">
                        <div class="card bg-light border-0 h-100">
                        <img src={this.imgSrc} className="card-img-top" alt="..." />
                        <div class="card-body">
                          <h5 class="card-title">Card title</h5>
                          <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                          <a href="#" class="btn btn-primary">Find Out More</a>
                        </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-xxl-3 mb-5">
                        <div class="card bg-light border-0 h-100">
                        <img src={this.imgSrc} className="card-img-top" alt="..." />
                        <div class="card-body">
                          <h5 class="card-title">Card title</h5>
                          <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                          <a href="#" class="btn btn-primary">Find Out More</a>
                        </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-xxl-3 mb-5">
                        <div class="card bg-light border-0 h-100">
                        <img src={this.imgSrc} className="card-img-top" alt="..." />
                        <div class="card-body">
                          <h5 class="card-title">Card title</h5>
                          <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                          <a href="#" class="btn btn-primary">Find Out More</a>
                        </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-xxl-3 mb-5">
                        <div class="card bg-light border-0 h-100">
                        <img src={this.imgSrc} className="card-img-top" alt="..." />
                        <div class="card-body">
                          <h5 class="card-title">Card title</h5>
                          <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                          <a href="#" class="btn btn-primary">Find Out More</a>
                        </div>
                        </div>
                    </div>
                </div>
              </div>
    )
  }
}
