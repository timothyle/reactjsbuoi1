import logo from './logo.svg';
import './App.css';
import BaiTapLayout from './layoutbaitap/BaiTapLayout';

function App() {
  return (
    <div className="App">
      <BaiTapLayout/>
    </div>
  );
}

export default App;
